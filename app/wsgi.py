from flask import Flask, Blueprint, request, jsonify
import os
from werkzeug.middleware.proxy_fix import ProxyFix
from src.db import model
from flask_migrate import Migrate
from src.api import schema
from src.api import endpoint
from src.api.endpoint import join
from sqlalchemy import text

# ====================================================
# Flask
# ====================================================
flask = Flask(__name__)
flask.wsgi_app = ProxyFix(flask.wsgi_app)

# ====================================================
# Database
# ====================================================
SQLALCHEMY_DATABASE_URI = (
    "postgresql+psycopg2://{user}:{password}@{host}/{name}".format(
        **{
            "user": "postgres",
            "password": "postgres",
            "host": "postgres",
            "name": "postgres",
        }
    )
)
# SQLALCHEMY_DATABASE_URI = "mysql+pymysql://{}:{}@{}/{}".format(
#     os.getenv("MARIADB_USER", "root"),
#     os.getenv("MARIADB_PASSWORD", "z9hG4bK"),
#     os.getenv("MARIADB_HOST", "mariadb"),
#     os.getenv("MARIADB_DATABASE", "mariadb"),
# )

flask.config["SQLALCHEMY_DATABASE_URI"] = SQLALCHEMY_DATABASE_URI
flask.config["SQLALCHEMY_ECHO"] = True

model.db.init_app(flask)
# model.db.create_all()

migrate = Migrate(flask, model.db)
migrate

# ====================================================
# Endpoint
# ====================================================
schema.api.add_namespace(endpoint.companies)
schema.api.add_namespace(endpoint.notifications)
schema.api.add_namespace(endpoint.product)
schema.api.add_namespace(join.company_product)
schema.api.add_namespace(endpoint.celerytest)
schema.api.init_app(flask)


# ====================================================
# HealthCheck"
# ====================================================
@flask.route("/health-check")
def flask_health_check():
    model.db.session.execute(text("SELECT 1"))
    return jsonify({})
