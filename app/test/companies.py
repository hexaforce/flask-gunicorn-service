import pytest
import requests
import json

company_id = None


@pytest.fixture
def api_url():
    return "http://localhost:5001"


@pytest.fixture
def headers():
    return {"Content-Type": "application/json"}


def test_create_company(api_url, headers):
    # POST /companies/
    payload = {"sf_account_id": "example_id"}
    response = requests.post(f"{api_url}/companies/", headers=headers, json=payload)
    assert response.status_code == 201
    data = response.json()
    assert "id" in data
    global company_id
    company_id = data["id"]


def test_list_companies(api_url, headers):
    # GET /companies/
    response = requests.get(f"{api_url}/companies/", headers=headers)
    assert response.status_code == 200
    data = response.json()
    assert isinstance(data, list)


def test_get_company(api_url, headers):
    # GET /companies/{id}
    response = requests.get(f"{api_url}/companies/{company_id}", headers=headers)
    assert response.status_code == 200
    data = response.json()
    assert "id" in data


def test_update_company(api_url, headers):
    # PUT /companies/{id}
    payload = {"sf_account_id": "updated_id"}
    response = requests.put(
        f"{api_url}/companies/{company_id}", headers=headers, json=payload
    )
    assert response.status_code == 200


def test_delete_company(api_url, headers):
    # DELETE /companies/{id}
    response = requests.delete(f"{api_url}/companies/{company_id}", headers=headers)
    assert response.status_code == 204
