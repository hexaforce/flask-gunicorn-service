from flask_restx.fields import Arbitrary
from flask_restx.fields import Boolean
from flask_restx.fields import ClassName
from flask_restx.fields import Date
from flask_restx.fields import DateTime
from flask_restx.fields import Fixed
from flask_restx.fields import Float
from flask_restx.fields import FormattedString
from flask_restx.fields import Integer
from flask_restx.fields import List
from flask_restx.fields import MarshallingError
from flask_restx.fields import MinMaxMixin
from flask_restx.fields import Nested
from flask_restx.fields import NumberMixin
from flask_restx.fields import Polymorph
from flask_restx.fields import Raw
from flask_restx.fields import String
from flask_restx.fields import StringMixin
from flask_restx.fields import Url
from flask_restx.fields import Wildcard

from src.api import schema

Companies = schema.api.model(
    "Companies",
    {
        "id": String(readonly=True, description="企業ID"),
        "sf_account_id": String(required=True, description="SF企業ID"),
        "updated_at": DateTime(readonly=True, description="更新日"),
        "created_at": DateTime(readonly=True, description="作成日"),
    },
)

Notifications = schema.api.model(
    "Notifications",
    {
        "id": String(readonly=True, description="お知らせID"),
        "target": String(required=True, description="対象者"),
        "icon": String(required=True, description="アイコン"),
        "message": String(required=True, description="メッセージ"),
        "read": Boolean(default=False, description="既読フラグ"),
        "link": String(required=False, description="ページリンク"),
        "updated_at": DateTime(readonly=True, description="更新日"),
        "created_at": DateTime(readonly=True, description="作成日"),
    },
)

Product = schema.api.model(
    "Product",
    {
        "id": String(readonly=True, description="お知らせID"),
        "product_name": String(required=True, description="商材名"),
        "company_id": String(required=True, description="企業ID"),
        "hearing_complete": Boolean(default=False, description="ヒアリング完了フラグ"),
        "updated_at": DateTime(readonly=True, description="更新日"),
        "created_at": DateTime(readonly=True, description="作成日"),
    },
)

CompanyProducts = schema.api.model(
    "CompanyProducts",
    {
        "company_id": String(readonly=True, description="お知らせID"),
        "sf_account_id": String(readonly=True, description="お知らせID"),
        "id": String(readonly=True, description="お知らせID"),
        "product_name": String(readonly=True, description="お知らせID"),
        "hearing_complete": Boolean(default=False, description="ヒアリング完了フラグ"),
        "created_at": DateTime(readonly=True, description="更新日"),
        "updated_at": DateTime(readonly=True, description="作成日"),
    },
)

CeleryTest = schema.api.model(
    "CeleryTest",
    {
        "message": String(required=True, description="お知らせID"),
    },
)
