from flask import request
from flask_restx import Resource

from src.api import schema
from src.db import crud
from src.db import model

from src.db.join.left import company_product as left_join
from src.db.join.inner import company_product as inner_join


company_product = schema.api.namespace(
    "company-product", description="CompanyProduct operations"
)


@company_product.route("/")
class CompanyProductList(Resource):
    """Shows a list of all CompanyProducts, and lets you POST to add new CompanyProduct"""

    @company_product.doc("list_company_product")
    @company_product.doc(
        params={
            "offset": {
                "description": "offset",
                "type": "integer",
                "default": 0,
            },
            "limit": {
                "description": "limit",
                "type": "integer",
                "default": 100,
            },
        }
    )
    @company_product.marshal_list_with(schema.CompanyProducts)
    def get(self):
        """List all CompanyProducts"""
        return left_join.all(
            offset=request.args.get("offset"), limit=request.args.get("limit")
        )


@company_product.route("/<id>")
class CompanyProductListByID(Resource):
    @company_product.doc("get_CompanyProducts")
    @company_product.doc(
        params={
            "offset": {
                "description": "offset",
                "type": "integer",
                "default": 0,
            },
            "limit": {
                "description": "limit",
                "type": "integer",
                "default": 100,
            },
        }
    )
    @company_product.marshal_list_with(schema.CompanyProducts)
    def get(self, id):
        """List all CompanyProducts"""
        return inner_join.getByID(
            id=id, offset=request.args.get("offset"), limit=request.args.get("limit")
        )
