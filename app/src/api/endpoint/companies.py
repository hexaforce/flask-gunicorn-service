from flask import request
from flask_restx import Resource

from src.api import schema
from src.db import crud
from src.db import model

companies = schema.api.namespace("companies", description="Companies operations")


@companies.route("/")
class CompaniesList(Resource):
    """Shows a list of all companiess, and lets you POST to add new companiess"""

    @companies.doc("create_companies")
    @companies.expect(schema.Companies)
    @companies.marshal_with(schema.Companies, code=201)
    def post(self):
        """Create a new companies"""
        return (
            crud.Companies.create(input=companies.payload),
            201,
        )

    @companies.doc("list_companiess")
    @companies.doc(
        params={
            "offset": {
                "description": "offset",
                "type": "integer",
                "default": 0,
            },
            "limit": {
                "description": "limit",
                "type": "integer",
                "default": 100,
            },
        }
    )
    @companies.marshal_list_with(schema.Companies)
    def get(self):
        """List all companiess"""
        return crud.Companies.read_all(
            offset=request.args.get("offset"), limit=request.args.get("limit")
        )


@companies.route("/<id>")
@companies.response(400, "Invalid parameter")
@companies.response(404, "Companies not found")
@companies.param("id", "The companies identifier")
class Companiess(Resource):
    """Show a single companies item and lets you delete them"""

    @companies.doc("get_companies")
    @companies.marshal_with(schema.Companies)
    def get(self, id):
        """Fetch a given resource"""
        return crud.Companies.read(id=id)

    @companies.doc("put_companies")
    @companies.expect(schema.Companies)
    @companies.marshal_with(schema.Companies)
    def put(self, id):
        """Update a companies given its identifier"""
        return crud.Companies.update(id=id, input=companies.payload)

    @companies.doc("delete_companies")
    @companies.response(204, "Companies deleted")
    def delete(self, id):
        """Delete a companies given its identifier"""
        crud.Companies.delete(id=id)
        return "", 204
