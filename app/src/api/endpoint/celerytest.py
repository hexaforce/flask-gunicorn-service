from flask import request
from flask_restx import Resource

from src.api import schema
from src.worker import worker
from src.worker import message

import random
import string

celerytest = schema.api.namespace("celerytest", description="CeleryTest operations")


def random_lower_string(k=32) -> str:
    return "".join(random.choices(string.ascii_lowercase, k=k))


@celerytest.route("/2")
class CeleryTestList(Resource):
    """Shows a list of all celerytests, and lets you POST to add new celerytests"""

    @celerytest.doc("create_celerytest2")
    @celerytest.expect(schema.CeleryTest)
    @celerytest.marshal_with(schema.CeleryTest, code=201)
    def post(self):
        """Create a new CeleryTest"""
        worker.send_task("src.worker.celery.test_celery2", args=[celerytest.payload])
        return message.CeleryTest(**celerytest.payload)


@celerytest.route("/3")
class CeleryTestList(Resource):
    """Shows a list of all celerytests, and lets you POST to add new celerytests"""

    @celerytest.doc("create_celerytest3")
    @celerytest.expect(schema.CeleryTest)
    @celerytest.marshal_with(schema.CeleryTest, code=201)
    def post(self):
        """Create a new CeleryTest"""
        worker.send_task("src.worker.celery.test_celery3", args=[celerytest.payload])
        return message.CeleryTest(**celerytest.payload)
