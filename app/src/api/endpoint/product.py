from flask import request
from flask_restx import Resource

from src.api import schema
from src.db import crud
from src.db import model

product = schema.api.namespace("product", description="Product operations")


@product.route("/")
class ProductList(Resource):
    """Shows a list of all products, and lets you POST to add new products"""

    @product.doc("create_product")
    @product.expect(schema.Product)
    @product.marshal_with(schema.Product, code=201)
    def post(self):
        """Create a new product"""
        return (
            crud.Product.create(input=product.payload),
            201,
        )

    @product.doc("list_products")
    @product.doc(
        params={
            "offset": {
                "description": "offset",
                "type": "integer",
                "default": 0,
            },
            "limit": {
                "description": "limit",
                "type": "integer",
                "default": 100,
            },
        }
    )
    @product.marshal_list_with(schema.Product)
    def get(self):
        """List all products"""
        return crud.Product.read_all(
            offset=request.args.get("offset"), limit=request.args.get("limit")
        )


@product.route("/<id>")
@product.response(400, "Invalid parameter")
@product.response(404, "Product not found")
@product.param("id", "The product identifier")
class Products(Resource):
    """Show a single product item and lets you delete them"""

    @product.doc("get_product")
    @product.marshal_with(schema.Product)
    def get(self, id):
        """Fetch a given resource"""
        return crud.Product.read(id=id)

    @product.doc("put_product")
    @product.expect(schema.Product)
    @product.marshal_with(schema.Product)
    def put(self, id):
        """Update a product given its identifier"""
        return crud.Product.update(id=id, input=product.payload)

    @product.doc("delete_product")
    @product.response(204, "Product deleted")
    def delete(self, id):
        """Delete a product given its identifier"""
        crud.Product.delete(id=id)
        return "", 204
