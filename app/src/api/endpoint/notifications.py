from flask import request
from flask_restx import Resource

from src.api import schema
from src.db import crud
from src.db import model

notifications = schema.api.namespace(
    "notifications", description="Notifications operations"
)


@notifications.route("/")
class NotificationsList(Resource):
    """Shows a list of all notificationss, and lets you POST to add new notificationss"""

    @notifications.doc("create_notifications")
    @notifications.expect(schema.Notifications)
    @notifications.marshal_with(schema.Notifications, code=201)
    def post(self):
        """Create a new notifications"""
        return (
            crud.Notifications.create(input=notifications.payload),
            201,
        )

    @notifications.doc("list_notificationss")
    @notifications.doc(
        params={
            "offset": {
                "description": "offset",
                "type": "integer",
                "default": 0,
            },
            "limit": {
                "description": "limit",
                "type": "integer",
                "default": 100,
            },
        }
    )
    @notifications.marshal_list_with(schema.Notifications)
    def get(self):
        """List all notificationss"""
        return crud.Notifications.read_all(
            offset=request.args.get("offset"), limit=request.args.get("limit")
        )


@notifications.route("/<id>")
@notifications.response(400, "Invalid parameter")
@notifications.response(404, "Notifications not found")
@notifications.param("id", "The notifications identifier")
class Notificationss(Resource):
    """Show a single notifications item and lets you delete them"""

    @notifications.doc("get_notifications")
    @notifications.marshal_with(schema.Notifications)
    def get(self, id):
        """Fetch a given resource"""
        return crud.Notifications.read(id=id)

    @notifications.doc("put_notifications")
    @notifications.expect(schema.Notifications)
    @notifications.marshal_with(schema.Notifications)
    def put(self, id):
        """Update a notifications given its identifier"""
        return crud.Notifications.update(id=id, input=notifications.payload)

    @notifications.doc("delete_notifications")
    @notifications.response(204, "Notifications deleted")
    def delete(self, id):
        """Delete a notifications given its identifier"""
        crud.Notifications.delete(id=id)
        return "", 204
