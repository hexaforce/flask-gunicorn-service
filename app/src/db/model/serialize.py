from sqlalchemy.inspection import inspect


class Serializer(object):
    def serialize(self):
        return {column: getattr(self, column) for column in inspect(self).attrs.keys()}

    @staticmethod
    def serialize_list(record_list):
        return [record.serialize() for record in record_list]
