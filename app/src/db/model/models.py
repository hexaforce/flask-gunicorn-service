# coding: utf-8
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()



class Company(db.Model):
    __tablename__ = 'companies'

    id = db.Column(db.Uuid, primary_key=True, server_default=db.FetchedValue(), info='企業ID')
    sf_account_id = db.Column(db.String, info='Salesforce企業ID')
    updated_at = db.Column(db.DateTime(True), server_default=db.FetchedValue())
    created_at = db.Column(db.DateTime(True), server_default=db.FetchedValue())



class CompanyInfo(db.Model):
    __tablename__ = 'company_info'

    id = db.Column(db.Uuid, primary_key=True, server_default=db.FetchedValue(), info='契約情報/企業情報ID')
    company_id = db.Column(db.ForeignKey('companies.id', ondelete='CASCADE', onupdate='CASCADE'), info='企業ID')
    corporate_name = db.Column(db.String)
    corporate_name_kana = db.Column(db.String)
    department_name = db.Column(db.String)
    person_in_charge_name = db.Column(db.String)
    person_in_charge_email = db.Column(db.String)
    mobile_number = db.Column(db.String)
    post_code = db.Column(db.String)
    address = db.Column(db.String)
    corporate_name_bill = db.Column(db.String)
    corporate_name_kana_bill = db.Column(db.String)
    department_name_bill = db.Column(db.String)
    person_in_charge_name_bill = db.Column(db.String)
    person_in_charge_email_bill = db.Column(db.String)
    mobile_number_bill = db.Column(db.String)
    post_code_bill = db.Column(db.String)
    address_bill = db.Column(db.String)
    billing_method_problem = db.Column(db.Boolean)
    billing_method_problem_text = db.Column(db.String)
    send_original_mail = db.Column(db.Boolean)
    approver = db.Column(db.String)
    project_manager = db.Column(db.String)
    creative_manager = db.Column(db.String)
    ads_operation_manager = db.Column(db.String)
    ads_operation_staff = db.Column(db.String)
    product_names = db.Column(db.ARRAY(db.String))
    product_priorities = db.Column(db.ARRAY(db.Integer))
    updated_at = db.Column(db.DateTime(True), server_default=db.FetchedValue())
    created_at = db.Column(db.DateTime(True), server_default=db.FetchedValue())

    company = db.relationship('Company', primaryjoin='CompanyInfo.company_id == Company.id', backref='company_infos')



class CustomerGroup(db.Model):
    __tablename__ = 'customer_groups'
    __table_args__ = (
        db.UniqueConstraint('opportunity_id', 'sf_contact_id'),
    )

    id = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue(), info='顧客グループID')
    opportunity_id = db.Column(db.ForeignKey('opportunities.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, info='商談ID')
    sf_contact_id = db.Column(db.String, nullable=False, info='SalesforceユーザID')
    email = db.Column(db.String, nullable=False, info='Cognito/Salesforceメール')
    username = db.Column(db.String, info='Cognit ID')

    opportunity = db.relationship('Opportunity', primaryjoin='CustomerGroup.opportunity_id == Opportunity.id', backref='customer_groups')



class MarketingActivityInformation(db.Model):
    __tablename__ = 'marketing_activity_information'

    id = db.Column(db.Uuid, primary_key=True, server_default=db.FetchedValue(), info='マーケティング活動情報ID')
    product_id = db.Column(db.ForeignKey('products.id', ondelete='CASCADE', onupdate='CASCADE'), info='プロダクトID')
    is_corporate = db.Column(db.Boolean)
    goal_calculation_done = db.Column(db.String)
    ad_monthly_budget = db.Column(db.String)
    ad_budget_ratio_listing_display = db.Column(db.String)
    listing_ad_budget_ratio = db.Column(db.String)
    display_ad_budget_ratio = db.Column(db.String)
    display_ad_budget_ratio_answer_status = db.Column(db.String)
    kaizen_frequency = db.Column(db.String)
    admin_comment_marketing_budget = db.Column(db.String)
    ad_media_decision_done = db.Column(db.String)
    have_plan_segment = db.Column(db.String)
    segment_delivery_method = db.Column(db.String)
    segment_materials_files = db.Column(db.ARRAY(db.String))
    segment_materials_file_keys = db.Column(db.ARRAY(db.String))
    segment_materials_answer_status = db.Column(db.String)
    admin_comment_delivery_schedule = db.Column(db.String)
    customer_conversion_corporate = db.Column(db.String)
    customer_conversion_individual = db.Column(db.String)
    CPA_definition = db.Column(db.String)
    admin_comment_CV_CPA_definition = db.Column(db.String)
    permission_result_ad_tool_view = db.Column(db.String)
    with_other_analysis_tool = db.Column(db.String)
    admin_comment_reference = db.Column(db.String)
    updated_at = db.Column(db.DateTime(True), server_default=db.FetchedValue())
    created_at = db.Column(db.DateTime(True), server_default=db.FetchedValue())

    product = db.relationship('Product', primaryjoin='MarketingActivityInformation.product_id == Product.id', backref='marketing_activity_information')



class Notification(db.Model):
    __tablename__ = 'notifications'

    id = db.Column(db.Uuid, primary_key=True, server_default=db.FetchedValue(), info='お知らせID')
    target = db.Column(db.String, nullable=False, info='対象者')
    icon = db.Column(db.String, nullable=False, info='アイコン')
    message = db.Column(db.String, nullable=False, info='メッセージ')
    read = db.Column(db.Boolean, server_default=db.FetchedValue(), info='既読フラグ')
    updated_at = db.Column(db.DateTime(True), server_default=db.FetchedValue())
    created_at = db.Column(db.DateTime(True), server_default=db.FetchedValue())
    link = db.Column(db.String, info='ページリンク')



class OperatorGroup(db.Model):
    __tablename__ = 'operator_groups'

    id = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue(), info='管理者グループID')
    opportunity_id = db.Column(db.ForeignKey('opportunities.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, info='商談ID')
    email = db.Column(db.String, nullable=False, info='担当者メールアドレス')
    username = db.Column(db.String, nullable=False, info='Cognito ID')
    department = db.Column(db.String, nullable=False, info='所属部署')
    name = db.Column(db.String, nullable=False, server_default=db.FetchedValue())

    opportunity = db.relationship('Opportunity', primaryjoin='OperatorGroup.opportunity_id == Opportunity.id', backref='operator_groups')



class OperatorName(db.Model):
    __tablename__ = 'operator_name'

    email = db.Column(db.String, primary_key=True)
    name = db.Column(db.String, nullable=False)



class Opportunity(db.Model):
    __tablename__ = 'opportunities'

    id = db.Column(db.Uuid, primary_key=True, server_default=db.FetchedValue(), info='商談ID')
    sf_opportunity_id = db.Column(db.String, info='Salesforce商談ID')
    product_id = db.Column(db.ForeignKey('products.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, info='商材ID')
    updated_at = db.Column(db.DateTime(True), server_default=db.FetchedValue())
    created_at = db.Column(db.DateTime(True), server_default=db.FetchedValue())
    sf_project_id = db.Column(db.String, nullable=False, info='Salesforce プロジェクトID')

    product = db.relationship('Product', primaryjoin='Opportunity.product_id == Product.id', backref='opportunities')



class ProductInformation(db.Model):
    __tablename__ = 'product_information'

    id = db.Column(db.Uuid, primary_key=True, server_default=db.FetchedValue(), info='商材情報ID')
    product_id = db.Column(db.ForeignKey('products.id', ondelete='CASCADE', onupdate='CASCADE'), info='プロダクトID')
    is_corporate = db.Column(db.Boolean)
    product_materials_files = db.Column(db.ARRAY(db.String))
    product_materials_file_keys = db.Column(db.ARRAY(db.String))
    product_answer_status = db.Column(db.String)
    products_features = db.Column(db.String)
    product_materials_url = db.Column(db.String)
    unique_strength = db.Column(db.String)
    admin_comment_product_overview = db.Column(db.String)
    ad_type_history = db.Column(db.String)
    existing_creative_files = db.Column(db.ARRAY(db.String))
    existing_creative_file_keys = db.Column(db.ARRAY(db.String))
    existing_creative_answer_status = db.Column(db.String)
    existing_creative_url = db.Column(db.String)
    article_LP_url = db.Column(db.String)
    admin_comment_past_productions = db.Column(db.String)
    superior_product = db.Column(db.String)
    similar_product = db.Column(db.String)
    referenced_product = db.Column(db.String)
    competitor_answer_status = db.Column(db.String)
    admin_comment_competitors = db.Column(db.String)
    product_achievement = db.Column(db.String)
    admin_comment_product_achievement = db.Column(db.String)
    campaign_offer = db.Column(db.String)
    admin_comment_campaign_offer = db.Column(db.String)
    success_case = db.Column(db.String)
    failure_case = db.Column(db.String)
    admin_comment_past_cases = db.Column(db.String)
    main_target = db.Column(db.String)
    customer_materials_files = db.Column(db.ARRAY(db.String))
    customer_materials_file_keys = db.Column(db.ARRAY(db.String))
    customer_materials_answer_status = db.Column(db.String)
    admin_comment_main_target = db.Column(db.String)
    individual_gender = db.Column(db.String)
    individual_age = db.Column(db.String)
    individual_occupation = db.Column(db.String)
    individual_family_income = db.Column(db.String)
    individual_family_structure = db.Column(db.String)
    individual_residence = db.Column(db.String)
    individual_hobby = db.Column(db.String)
    individual_hobby_expense = db.Column(db.String)
    individual_problem = db.Column(db.String)
    individual_owned_device = db.Column(db.String)
    individual_SNS_usually = db.Column(db.String)
    individual_internet_usage = db.Column(db.String)
    admin_comment_individual = db.Column(db.String)
    corporate_job_title = db.Column(db.String)
    corporate_industry = db.Column(db.String)
    corporate_problem = db.Column(db.String)
    admin_comment_target_supplement = db.Column(db.String)
    device_impression_ratio = db.Column(db.String)
    sub_target_existence = db.Column(db.String)
    admin_comment_corporate = db.Column(db.String)
    updated_at = db.Column(db.DateTime(True), server_default=db.FetchedValue())
    created_at = db.Column(db.DateTime(True), server_default=db.FetchedValue())

    product = db.relationship('Product', primaryjoin='ProductInformation.product_id == Product.id', backref='product_information')



class Product(db.Model):
    __tablename__ = 'products'
    __table_args__ = (
        db.UniqueConstraint('company_id', 'product_name'),
    )

    id = db.Column(db.Uuid, primary_key=True, server_default=db.FetchedValue(), info='商材ID')
    product_name = db.Column(db.String, info='商材名')
    company_id = db.Column(db.ForeignKey('companies.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, info='企業ID')
    updated_at = db.Column(db.DateTime(True), server_default=db.FetchedValue())
    created_at = db.Column(db.DateTime(True), server_default=db.FetchedValue())
    hearing_complete = db.Column(db.Boolean, server_default=db.FetchedValue(), info='ヒアリング完了フラグ')

    company = db.relationship('Company', primaryjoin='Product.company_id == Company.id', backref='products')



class RequestContent(db.Model):
    __tablename__ = 'request_content'

    id = db.Column(db.Uuid, primary_key=True, server_default=db.FetchedValue(), info='ご依頼内容ID')
    product_id = db.Column(db.ForeignKey('products.id', ondelete='CASCADE', onupdate='CASCADE'), info='プロダクトID')
    is_corporate = db.Column(db.Boolean)
    order_background = db.Column(db.String)
    any_additional_comments = db.Column(db.String)
    result_ad_file_keys = db.Column(db.ARRAY(db.String))
    result_ad_files = db.Column(db.ARRAY(db.String))
    result_ad_answer_status = db.Column(db.String)
    qualitative_issue = db.Column(db.String)
    quantitative_issue = db.Column(db.String)
    admin_comment_order_background = db.Column(db.String)
    qualitative_goal = db.Column(db.String)
    quantitative_goal_answer_status = db.Column(db.String)
    quantitative_goal = db.Column(db.String)
    admin_comment_issue = db.Column(db.String)
    admin_comment_goal = db.Column(db.String)
    request_first_creative_type = db.Column(db.String)
    creative_preferred_plan = db.Column(db.String)
    admin_comment_first_production = db.Column(db.String)
    reason_for_change_existing_creative = db.Column(db.String)
    creative_regulation_materials_file_keys = db.Column(db.ARRAY(db.String))
    creative_regulation_materials_files = db.Column(db.ARRAY(db.String))
    creative_materials_file_names = db.Column(db.ARRAY(db.String))
    creative_materials_file_keys = db.Column(db.ARRAY(db.String))
    LP_external_service_existence = db.Column(db.String)
    creative_materials_answer_status = db.Column(db.String)
    creative_regulation_url_or_text = db.Column(db.String)
    creative_regulation_answer_status = db.Column(db.String)
    admin_comment_regulation = db.Column(db.String)
    updated_at = db.Column(db.DateTime(True), server_default=db.FetchedValue())
    created_at = db.Column(db.DateTime(True), server_default=db.FetchedValue())

    product = db.relationship('Product', primaryjoin='RequestContent.product_id == Product.id', backref='request_contents')



class UserInformationMemo(db.Model):
    __tablename__ = 'user_information_memo'

    id = db.Column(db.Uuid, primary_key=True, server_default=db.FetchedValue(), info='ご依頼内容ID')
    product_id = db.Column(db.ForeignKey('products.id', ondelete='CASCADE', onupdate='CASCADE'), info='プロダクトID')
    memo = db.Column(db.String)
    admin_memo = db.Column(db.String)
    updated_at = db.Column(db.DateTime(True), server_default=db.FetchedValue())
    created_at = db.Column(db.DateTime(True), server_default=db.FetchedValue())

    product = db.relationship('Product', primaryjoin='UserInformationMemo.product_id == Product.id', backref='user_information_memos')
