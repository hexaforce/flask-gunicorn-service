from typing import Optional

from pydantic import BaseModel

from src.db import crud
from src.db import model


class CompaniesBase(BaseModel):
    pass


class CompaniesCreate(CompaniesBase):
    sf_account_id: str


class CompaniesUpdate(CompaniesBase):
    sf_account_id: Optional[str]


class CRUDCompanies(crud.CRUDBase[model.Companies, CompaniesCreate, CompaniesUpdate]):
    pass


Companies = CRUDCompanies(model.Companies)
