from typing import Optional

from pydantic import BaseModel

from src.db import crud
from src.db import model

from uuid import UUID


class ProductBase(BaseModel):
    pass


class ProductCreate(ProductBase):
    product_name: str
    company_id: UUID
    hearing_complete: bool


class ProductUpdate(ProductBase):
    product_name: Optional[str]
    company_id: Optional[UUID]
    hearing_complete: Optional[bool]


class CRUDProduct(crud.CRUDBase[model.Product, ProductCreate, ProductUpdate]):
    pass


Product = CRUDProduct(model.Product)
