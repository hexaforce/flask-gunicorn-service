from typing import Optional

from pydantic import BaseModel

from src.db import crud
from src.db import model


class NotificationsBase(BaseModel):
    pass


class NotificationsCreate(NotificationsBase):
    target: str
    icon: str
    message: str
    read: bool


class NotificationsUpdate(NotificationsBase):
    target: Optional[str]
    icon: Optional[str]
    message: Optional[str]
    read: Optional[bool]


class CRUDNotifications(
    crud.CRUDBase[model.Notifications, NotificationsCreate, NotificationsUpdate]
):
    pass


Notifications = CRUDNotifications(model.Notifications)
