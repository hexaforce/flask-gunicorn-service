from src.db import crud
from src.db import model
from typing import Any
from typing import Dict
from typing import List
from typing import Set
from sqlalchemy import desc


def getByID(id: Any, offset: int = 0, limit: int = 100) -> List[Any]:
    return (
        model.db.session.query(
            model.Companies.sf_account_id,
            model.Product.id,
            model.Product.company_id,
            model.Product.product_name,
            model.Product.hearing_complete,
            model.Product.created_at,
            model.Product.updated_at,
        )
        .join(
            model.Companies,
            model.Companies.id == model.Product.company_id,
        )
        .filter(model.Companies.id == id)
        .order_by(desc(model.Product.created_at))
        .offset(offset)
        .limit(limit)
        .all()
    )
