from pydantic import BaseModel


class CeleryTestBase(BaseModel):
    pass


class CeleryTest(CeleryTestBase):
    message: str
