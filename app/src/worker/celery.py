from raven import Client

from src.worker import message

from celery import Celery

worker = Celery(
    "worker",
    broker="amqp://guest@rabbitmq//",
    backend="db+postgresql://postgres:postgres@postgres:5432/postgres",
)

worker.conf.task_routes = (
    [
        ("src.worker.celery.test_celery2", {"queue": "test-queue"}),
        ("src.worker.celery.test_celery3", {"queue": "mail-queue"}),
    ],
)

# client_sentry = Client(settings.SENTRY_DSN)


@worker.task(acks_late=True)
def test_celery2(payload: dict) -> str:
    queueMessage = message.CeleryTest(**payload)
    return f"test task return {queueMessage.message}"


@worker.task(acks_late=True)
def test_celery3(payload: dict) -> str:
    queueMessage = message.CeleryTest(**payload)
    return f"test task return {queueMessage.message}"
