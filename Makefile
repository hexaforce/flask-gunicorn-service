show:
	docker ps
	docker volume ls
	docker network ls
	docker images

upf:
	docker compose up --build

up:
	docker compose up -d --build

down:
	docker compose down

test:
	docker compose exec -T api pytest ./app/tests

destroy:
	docker system prune -a --volumes

log:
	docker compose logs -f

